---
title: 夏令时问题
date: 2019-10-01 16:30:30
tags:
- 夏令时
---

在工作中，遇到一个很有意思的关于时间的问题，这里跟大家分享一下。

## 现象

从数据库里取出用户的生日`1990-05-22`，接口返回时发现返回值变成了`1990-05-21`，而对于`1992-05-22`这样的日期却能正确返回。

网上很多对于相关问题的分析都是简单粗暴的认为是时区的问题，却没有讲明白问题出现的根本原因。

## 排查过程

### 问题复现

为了复现问题现象，我们先快速搭建一个 Spring Boot Application。

先将配置文件中对日期类型的格式化配置为 `yyyy-MM-dd`，如下所示：

```properties
spring.jackson.time-zone=GMT+8
spring.jackson.date-format=yyyy-MM-dd
```

写一个接口，直接将有问题的日期输出，代码如下：

```java
@RestController
public class IndexController {

    @GetMapping("/dst")
    public Map<String, Object> dst(@RequestParam("date") Date date) {
        Map<String, Object> map = Maps.newHashMap();
        System.out.println("birthday : " + DateFormatUtils.format(date, "yyyy-MM-dd"));
        map.put("birthday", date);
        return map;
    }
}
```

构造如下请求：

```http
GET http://localhost:8081/dst?date=1990/05/22
```

发现服务端日志正确输出`1990-05-22`，而接口返回值却输出：

```json
{"birthday": "1990-05-21"}
```

构造如下请求：

```http
GET http://localhost:8081/dst?date=1992/05/22
```

发现服务端日志正确输出`1992-05-22`，此时接口的输出与服务端日志一致：

```json
{"birthday": "1992-05-22"}
```

至此，我们复现了本文要讲解的问题。

### 输出对应的时间

为了更精确的知道这两个日期究竟有什么不同，我们更详细的输出这两个 `Date` 所对应的具体时间。

修改配置文件为：

```properties
spring.jackson.date-format=yyyy-MM-dd HH:mm:ss
```

重复以上请求，发现对于`1990-05-22`，服务端日志输出`1990-05-22 00:00:00`，而接口返回值则为`1990-05-21 23:00:00`，两者相差了一个小时。而对于`1992-05-22`，服务端日志与接口返回值完全一致。

### 输出系统的时区

通过上述的现象基本上可以定位问题出现在 `Jackson` 对日期的序列化上。

为了排查这个问题，我们在启动 Spring Boot Application 时，先输出当前系统的时区，发现系统时区为 `Asia/Shanghai`，而我们配置的 `Jackson` 时区为 `GMT+8`。

难道 `Asia/Shanghai` 与 `GMT+8` 不是同一个时区？

### 求证

为了证明我们的猜想是正确的，在启动 Spring Boot Application 时，将系统时区设置为 `GMT+8`。

```java
@SpringBootApplication
public class DSTApplication {

    public static void main(String[] args) {
        SpringApplication.run(DSTApplication.class, args);
        TimeZone.setDefault(TimeZone.getTimeZone("GMT+8"));
    }
}
```

重复以上请求，发现对于`1990-05-22`，服务端日志和接口均正确输出 `1990-05-22 00:00:00`。

若不设置默认时区为 `GMT+8`，而是将 `Jackson` 序列化的时区配置修改为 `Asia/Shanghai`，接口也能正确输出。

那么 `Asia/Shanghai` 与 `GMT+8` 在什么情况下会导致相差一个小时的问题呢？

## 夏令时

> 夏令时，表示为了节约能源，人为规定时间的意思。也叫夏时制，夏时令（Daylight Saving Time：DST），又称“日光节约时制”和“夏令时间”，在这一制度实行期间所采用的统一时间称为“夏令时间”。一般在天亮早的夏季人为将时间调快一小时，可以使人早起早睡，减少照明量，以充分利用光照资源，从而节约照明用电。各个采纳夏时制的国家具体规定不同。目前全世界有近110个国家每年要实行夏令时。1986年4月，中国中央有关部门发出“在全国范围内实行夏时制的通知”，具体作法是：每年从四月中旬第一个星期日的凌晨2时整（北京时间），将时钟拨快一小时，即将表针由2时拨至3时，夏令时开始；到九月中旬第一个星期日的凌晨2时整（北京夏令时），再将时钟拨回一小时，即将表针由2时拨至1时，夏令时结束。从1986年到1991年的六个年度，除1986年因是实行夏时制的第一年，从5月4日开始到9月14日结束外，其它年份均按规定的时段施行。在夏令时开始和结束前几天，新闻媒体均刊登有关部门的通告。1992年起，夏令时暂停实行。

以上内容摘抄自[百度百科](https://baike.baidu.com/item/%E5%A4%8F%E4%BB%A4%E6%97%B6)，这也就是为什么在上述例子中为什么`1992-05-22`即使在系统时区与 `Jackson` 序列化时区不一致的情况下，仍然能够正确返回，而`1990-05-22`就会出错。