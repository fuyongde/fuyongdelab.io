---
title: JVM 手册
date: 2019-11-10 16:34:15
categories:
  - 手册
tags:
  - JVM
  - 未完待续
---

记录常用的 JVM 参数，持续更新 ...

## 堆内存设置

`-Xms128M` : 设置 java 程序启动时堆内存 128M（默认为物理内存 1/64,且小于 1G），阿里 p3c 文档中推荐 Xmx 与 Xms 配置同样的大小，避免 GC 后调整堆大小带来的压力

`-Xmx256M` : 设置最大堆内存 256M，超出后会出现 OutOfMemoryError（默认为物理内存 1/64，且小于 1G）

`-Xmn10M` : 设置新生代区域大小为 10M

`-XX:NewSize=2M` : 设置新生代初始大小为 2M

`-XX:MaxNewSize=2M` : 设置新生代最大值为 2M

`-Xss1M` : 设置线程栈的大小 1M（默认 1M）

`-XX:MinHeapFreeRatio=40` : 设置堆空间最小空闲比例（默认 40）（当 Xmx 与 Xms 相等时，该配置无效）

`-XX:MaxHeapFreeRatio=70` : 设置堆空间最大空闲比例（默认 70）（当 Xmx 与 Xms 相等时，该配置无效）

`-XX:NewRatio=2` : 设置年轻代与年老代的比例为 2:1

`-XX:SurvivorRatio=8` : 设置年轻代中 eden 区与 survivor 区的比例为 8 : 1

`-XX:MetaspaceSize=64M` : 设置元数据空间初始大小（取代-XX:PermSize）

`-XX:MaxMetaspaceSize=128M` : 设置元数据空间最大值（取代之前-XX:MaxPermSize）

`-XX:TargetSurvivorRatio=50` : 设置 survivor 区使用率。当 survivor 区达到 50%时，将对象送入老年代

`-XX:+UseTLAB` : 在年轻代空间中使用本地线程分配缓冲区(TLAB)，默认开启

`-XX:TLABSize=512k` : 设置 TLAB 大小为 512k

`-XX:+UseCompressedOops` : 使用压缩指针，默认开启

`-XX:MaxTenuringThreshold=15` : 对象进入老年代的年龄（Parallel 是 15，CMS 是 6）

## 垃圾收集器相关参数

`-XX:+UseSerialGC` : 启用 `Serial` 收集器，开启之后会默认启用 Serial（年轻代）+ Serial Old（年老代）的收集器组合。

`-XX:+UseParNewGC` : 启用 `ParNew` 收集器，开启之后会默认启用 ParNew（年轻代）+ Serial Old（老年代，该收集器已过时）的收集器组合，新生代使用**复制**算法，老年代采用**标记-整理**算法。推荐配合老年代使用 `CMS` 收集器。

`-XX:+UseParallelGC` : 启用 `Parallel` 收集器，Java8 默认启用该收集器，俗称吞吐量优先的收集器。开启之后默认启用 Parallel Scavenge（年轻代）+ Parallel Old（老年代）。与 ParNew 的区别在于自适应调节策略，虚拟机会根据当前系统的运行情况手机性能监控信息，动态调整参数以提供最合适的停顿时间或最大吞吐量。新生代采用**复制**算法，老年代采用**标记-整理**算法。

`-XX:+UseParallelOleGC` : 启用 `Parallel` 收集器，与 `-XX:+UseParallelGC` 可相互激活。

`-XX:+ParallelGCThreads` : 限制垃圾收集的线程数量，默认开始和 CPU 数量相同的线程数。

`-XX:+UseConcMarkSweepGC` : 启用 `CMS` 收集器，开启该参数之后，默认打开 `-XX:+UseParNewGC`，启用 ParNew（年轻代）+ CMS（老年代）+ Serial Old（老年代）收集器的组合，Serial Old 将作为 CMS 出错的后备收集器。

`-XX:+UseG1GC` : 启用 `G1` 收集器，`G1` 收集器不同于以往的收集器，`G1` 收集器既可以回收年轻代的垃圾也可以回收年老代的垃圾。

## GC 相关的参数

`-XX:+PrintGC` : 输出 GC 日志

`-XX:+PrintGCDetails` : 输出 GC 的详细日志

`-XX:+PrintGCTimeStamps` : 输出 GC 的时间戳（以基准时间的形式）

`-XX:+PrintGCDateStamps` : 输出 GC 的时间戳（以日期的形式，如 2013-05-04T21:53:59.234+0800）

`-XX:+PrintHeapAtGC` : 在进行 GC 的前后打印出堆的信息

`-XX:+PrintGCApplicationStoppedTime` : 输出 GC 造成应用暂停的时间

`-Xloggc:../logs/gc.log` : 日志文件的输出路径

## 其他

`-XX:+HeapDumpOnOutOfMemoryError` : 让 JVM 碰到 OOM 场景时输出 dump 信息。 说明：OOM 的发生是有概率的，甚至有规律地相隔数月才出现一例，出现时的现场信息对查错 非常有价值。

`-XX:MaxGCPauseMillis` : 设置每次年轻代垃圾回收的最长时间，如果无法满足此时间，JVM 会自动调整年轻代大小，以满足此值。

`-XX:+TraceClassLoading` : 跟踪类的加载信息

`-XX:+PrintCommandLineFlags` : 打印启动命令参数
