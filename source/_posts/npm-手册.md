---
title: npm-手册
date: 2020-07-19 18:46:48
categories:
  - 手册
tags:
  - npm
  - 未完待续
---

## 配置 npmmirror 镜像

1. 备份旧镜像

在替换 npmmirror 镜像之前，我们先查看一下原有的镜像

```shell
npm get registry
```

结果为 `https://registry.npmjs.org/`

```shell
yarn get registry
```

结果为 `https://registry.yarnpkg.com`

2. 替换为 npmmirror 镜像

```shell
# 设置 npm 的源为 npmmirror
npm config set registry https://registry.npmmirror.com
# 设置 yarn 的源为 npmmirror
yarn config set registry https://registry.npmmirror.com
```

## 升级版本

### 查看 npm 版本

```shell
npm -v
```

### 升级到最新的 npm 版本

```shell
npm install -g npm
```

### 安装指定版本的 npm

```shell
npm install -g npm@版本号
```
