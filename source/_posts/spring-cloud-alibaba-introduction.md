---
title: Spring Cloud Alibaba Introduction
date: 2019-10-11 16:04:23
tags:
  - Spring Cloud
  - Spring Cloud Alibaba
  - Dubbo
---

## 介绍

> `Spring Cloud Alibaba` 致力于提供微服务开发的一站式解决方案。此项目包含开发分布式应用服务的必需组件（涵盖了服务治理、配置管理、限流降级以及阿里开源生态和商业生态的诸多组件），方便开发者通过 `Spring Cloud` 编程模型轻松使用这些组件来开发分布式应用服务。依托 `Spring Cloud Alibaba`，您只需要添加一些注解和少量配置，就可以将 `Spring Cloud` 应用接入阿里分布式应用解决方案，通过阿里中间件来迅速搭建分布式应用系统。

### Spring Cloud 是什么

> Spring Cloud provides tools for developers to quickly build some of the common patterns in distributed systems .

`Spring Cloud` 为开发者提供了快速构建分布式系统的模型。`Spring Cloud` 除了抽象了分布式系统里的一些接口，并且提供了一系列开箱即用的组件。

`Spring Cloud` 更详细的介绍以及相关的组件，可以参考[官网](https://spring.io/projects/spring-cloud)。

### Spring Cloud Alibaba 对 Spring Cloud 的意义

- `Spring Cloud Alibaba` 在 `Spring Cloud` 体系中是一个套件，类似于 `Netflix OSS`，为开发者提供了更多的选择。
- 在 `Netflix OSS` 诸多重要组件先后宣布停止新功能开发的大背景下 `Spring Cloud Alibaba` 作为一个高速迭代中的新生项目无疑是更合适的选择。
- 将国内非常流行的 `Dubbo`、`RocketMQ` 等融入 `Spring Cloud` 体系，使得应用的接入和改造的成本更低。
- `Spring Cloud Alibaba` 是中文文档最全的 `Spring Cloud` 组件。

## 组件

### 开源组件

- Nacos Config：配置中心
- Nacos Discovery：服务发现（注册中心）
- Sentinel：把流量作为切入点，从流量控制、熔断降级、系统负载保护等多个维度保护服务的稳定性。
- RocketMQ：`Apache RocketMQ™` 基于 Java 的高性能、高吞吐量的分布式消息和流计算平台。
- Dubbo Spring Cloud：`Apache Dubbo™` 是一款高性能 Java RPC 框架。
- Seata：阿里巴巴开源产品，一个易于使用的高性能微服务分布式事务解决方案。

### 商业化组件

- ANS：注册中心
- ACM：配置中心
- OSS：阿里云对象存储服务
- SchedulerX：阿里中间件团队开发的一款分布式任务调度产品，支持周期性的任务与固定时间点触发任务。
- SMS：覆盖全球的短信服务，友好、高效、智能的互联化通讯能力，帮助企业迅速搭建客户触达通道。

### 组件的版本关系

| Spring Cloud Alibaba Version              | Sentinel Version | Nacos Version | RocketMQ Version | Dubbo Version | Seata Version |
| ----------------------------------------- | ---------------- | ------------- | ---------------- | ------------- | ------------- |
| 2.1.0.RELEASE 2.0.0.RELEASE 1.5.0.RELEASE | 1.6.3            | 1.1.1         | 4.4.0            | 2.7.3         | 0.7.1         |
| 0.9.0.RELEASE 0.2.2.RELEASE 0.1.2.RELEASE | 1.5.2            | 1.0.0         | 4.4.0            | 2.7.1         | 0.4.2         |
| 0.2.1.RELEASE 0.1.1.RELEASE               | 1.4.0            | 0.6.2         | 4.3.1            | x             | x             |
| 0.2.0.RELEASE 0.1.0.RELEASE               | 1.3.0-GA         | 0.3.0         | x                | x             | x             |

### 与 Spring Cloud 以及 Spring Boot 的版本依赖关系

| Spring Cloud Version   | Spring Cloud Alibaba Version | Spring Boot Version |
| ---------------------- | ---------------------------- | ------------------- |
| Spring Cloud Greenwich | 2.1.0.RELEASE                | 2.1.X.RELEASE       |
| Spring Cloud Finchley  | 2.0.0.RELEASE                | 2.0.X.RELEASE       |
| Spring Cloud Edgware   | 1.5.0.RELEASE                | 1.5.X.RELEASE       |

过老的版本不推荐

## Spring Cloud Alibaba VS Dubbo

`Dubbo Spring Cloud` (归属于 Spring Cloud Alibaba 项目)是基于 `Dubbo Spring Boot` (归属于 Dubbo 项目)和 `Spring Cloud`开发，使得 Dubbo 应用以一种更优雅的方式融入 `Spring Cloud` 体系。

### 功能对比

由于 `Dubbo Spring Cloud` 构建在原生的 `Spring Cloud` 之上，其服务治理方面的能力可认为是 `Spring Cloud` 的加强版，不仅完全覆盖 `Spring Cloud` [原生特性](https://cloud.spring.io/spring-cloud-static/Greenwich.RELEASE/single/spring-cloud.html#_features)，而且提供更为稳定和成熟的实现，特性比对如下表所示：

| 功能组件                                             | Spring Cloud                                                                                                                 | Dubbo Spring Cloud                                                                                                                 | Dubbo                                           |
| ---------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- |
| 分布式配置（Distributed configuration）              | Git、Zookeeper、Consul、JDBC                                                                                                 | Spring Cloud 分布式配置 + [Dubbo 配置中心](http://dubbo.apache.org/zh-cn/docs/user/configuration/config-center.html)               | Dubbo 配置中心                                  |
| 服务注册与发现（Service registration and discovery） | Eureka、Zookeeper、Consul                                                                                                    | Spring Cloud 原生注册中心[1] + [Dubbo 原生注册中心](http://dubbo.apache.org/zh-cn/docs/user/references/registry/introduction.html) | Dubbo 原生注册中心                              |
| 负载均衡（Load balancing）                           | Ribbon（随机、轮询等算法）                                                                                                   | Dubbo 内建实现（随机、轮询等算法 + 权重等特性）[2]                                                                                 | Dubbo 内建实现（随机、轮询等算法 + 权重等特性） |
| 服务熔断（Circuit Breakers）                         | Spring Cloud Hystrix                                                                                                         | Spring Cloud Hystrix + Alibaba Sentinel[3] 等                                                                                      | Alibaba Sentinel                                |
| 服务调用（Service-to-service calls）                 | Open Feign、`RestTemplate`                                                                                                   | Spring Cloud 服务调用 + Dubbo `@Reference`                                                                                         | Dubbo `@Reference`                              |
| 链路跟踪（Tracing）                                  | [Spring Cloud Sleuth](https://spring.io/projects/spring-cloud-sleuth) + [Zipkin](https://github.com/apache/incubator-zipkin) | Zipkin、opentracing 等                                                                                                             | Zipkin、opentracing 等                          |

[1]: `Spring Cloud` 原生注册中心，除 `Eureka`、`Zookeeper`、`Consul` 之外，还包括 `Spring Cloud Alibaba` 中的 `Nacos`

[2]: `Dubbo Spring Cloud` 相对于单纯的 `Dubbo` 服务的一个坑：单纯的 `Dubbo` 是将每一个由 @org.apache.dubbo.config.annotation.Service 注解的服务作为单独的服务暴露的，`Dubbo Spring Cloud` 如果采用推荐的写法，是将所属的工程作为一个服务来暴露的，若同一个服务版本不一致，会导致消费者找不到服务的情况。

[3]: [Alibaba Sentinel](https://github.com/alibaba/Sentinel/wiki/%E4%BB%8B%E7%BB%8D)：`Sentinel` 以流量为切入点，从流量控制、熔断降级、系统负载保护等多个维度保护服务的稳定性 - ，目前 `Sentinel` 已被 `Spring Cloud` 项目纳为[断路器的候选实现](https://spring.io/blog/2019/04/16/introducing-spring-cloud-circuit-breaker)

### 写法对比

从依赖上，`Dubbo Spring Boot` 的项目需要引入 `dubbo-spring-boot-starter` 相关依赖，`Dubbo Spring Cloud` 项目需要引入 `spring-cloud-starter-dubbo`相关依赖。

Example：

`Dubbo Spring Boot`

```xml
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo-spring-boot-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo</artifactId>
</dependency>
<dependency>
    <groupId>org.apache.dubbo</groupId>
    <artifactId>dubbo-registry-nacos</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-client</artifactId>
</dependency>
```

`Dubbo Spring Cloud`

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-dubbo</artifactId>
</dependency>
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
</dependency>
```

从配置和代码上，`Dubbo Spring Cloud` 完全兼容 `Dubbo Spring Boot` 的配置写法，但是更推荐将 `Dubbo` 挂载本机 `Spring Cloud` 的服务发现的地址，并且推荐配置消费者所订阅的服务，从而来降低服务寻址时的 CPU 和内存开销。写发如下：

```yaml
spring:
  cloud:
    nacos:
      discovery:
        server-addr: nacos1.dafy.com:8848
dubbo:
  registry:
    address: spring-cloud://localhost
  cloud:
    subscribed-services: bs-auth
```
