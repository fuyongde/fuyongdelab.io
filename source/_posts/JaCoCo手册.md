---
title: JaCoCo手册
date: 2022-01-26 23:09:52
tags:
---

## JaCoCo 简介

> JaCoCo should provide the standard technology for code coverage analysis in Java VM based environments. The focus is providing a lightweight, flexible and well documented library for integration with various build and development tools.

JaCoCo 要为基于 Java VM 的环境中的代码提供覆盖率分析标准技术，重点是提供一个轻量、灵活且文档齐全的库，以便与各种构建和开发工具集成。

### 功能特性

- 指令（C0）、分支（C1）、行、方法、类型、圈复杂度的覆盖率分析；
- 基于 Java 字节码，即使没有源码依然可以运行；
- 支持 on-the-fly 模式，可以通过 javaagent 便捷地集成，也可以通过 API 来自定义类装载器进行集成；
- 与框架无关，可以对基于 Java VM 的应用程序顺利的集成；
- 与已所有已发布的 Java 版本兼容；
- 支持不同的 JVM 语言；
- 可以生成多种格式的报告（HTML、XML、CSV）；
- 支持远程协议和 JMX 控制，可以在任何时间点来进行覆盖率数据的存储；

### 使用

下载完 JaCoCo 之后会得到几个 jar 包，其中 jacocoagent.jar 和 jacococli.jar 是我们比较关注的。

- jacocoagent.jar 是 JVM 应用程序启动时的代理
- jacococli.jar 是生成覆盖率报告、合并 dump 文件的命令行工具

#### jacocoagent

Java Agent 的原理可以阅读 `java.lang.instrument.Instrumentation` 这个类，这里先介绍 jacocoagent 的使用方法。

在 Java 应用的启动命令上加入如下的参数：

`-javaagent:[yourpath/]jacocoagent.jar=[option1]=[value1],[option2]=[value2]`

即可对启动的 Java 应用程序中指定的类进行代码覆盖率的采集，其中 option 的选项有：

| option                | 缺省值             | 描述                                                         |
| --------------------- | ------------------ | ------------------------------------------------------------ |
| destfile              | jacoco.exec        | 保存 jacoco 采集的数据文件的路径                             |
| append                | true               | 数据文件保存路径的文件存在的情况下，是否追加覆盖率数据       |
| includes              | \*                 | 采集哪些类（采集类过多会导致新能下降严重）                   |
| excludes              | \*                 | 排除哪些类                                                   |
| inclbootstrapclasses  | false              | 是否需要采集 bootstrap classLoader 加载的类                  |
| inclnolocationclasses | false              | 是否需要采集没有原位置的类（即程序运行时生成的类）           |
| sessionid             | auto-generated     | 会话标识符，如果没有配置该参数，jacocoagent 会自动生成       |
| dumponexit            | true               | 是否在 VM 关闭时写入数据                                     |
| output                | file               | 存储数据的方式，有效的选项：file、tcpserver、tcpclient、none |
| address               | loopback interface | 当 output 为 tcpserver 时启用，缺省值为 127.0.0.1            |
| port                  | 6300               | 当 output 为 tcpserver 时启用                                |
| classdumpdir          | no dumps           | 保存插桩之后的 class 的路径                                  |
| jmx                   | false              | 是否开启 JMX 功能                                            |

#### jacococli

jacoco 的命令行工具，可以用于链接 jacocoagent 用于采集回文件的存储、对指定的类进行插桩、合并 dump 文件、生成报告等功能，具体使用方法不再一一列举，可以查看[官方文档](https://www.jacoco.org/jacoco/trunk/doc/cli.html)
