---
title: Jenkins-手册
date: 2020-02-18 16:54:55
categories:
  - 手册
tags:
  - Jenkins
  - CI
  - 持续集成
  - 未完待续
---

Jenkins 是常见的 CI 平台

## 安装

### Windows 平台

略

### CentOS 平台

更为详细的步骤及说明可查看[官网](https://pkg.jenkins.io/redhat-stable/)

#### 添加 jenkins 源

命令如下：

```shell
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
```

#### yum 安装

命令如下：

```shell
# 在线安装的方式
yum install jenkins

# 离线安装的方式，由于国内连接 jenkins 网站较慢，可先行下载 rpm 文件格式的离线安装包，采用离线的方式安装
yum install jenkins-2.204.2-1.1.noarch.rpm
```

#### 启动

命令如下：

```shell
systemctl start jenkins
```

#### 解决插件中心安装慢的问题

第一次登陆 Jenkins 会出现初始化缓慢的问题，产生这个问题的原因有两个，一个是因为首次打开 Jenkins 会尝试连接 Google，另外一个原因是因为连接插件中心缓慢。

##### 将插件中心修改为清华的源

修改文件 `$JENKINS_HOME/hudson.model.UpdateCenter.xml`，将 url 从 `https://updates.jenkins.io/update-center.json` 修改为 `https://mirrors.tuna.tsinghua.edu.cn/jenkins/updates/update-center.json`

##### 将 Google 的地址修改为 Baidu

修改文件 `$JENKINS_HOME/updates/default.json`

- 将 `connectionCheckUrl` 的值修改为 `http://www.baidu.com/`
- 替换文件中 `updates.jenkins-ci.org/download` 为 `mirrors.tuna.tsinghua.edu.cn/jenkins`

未完待续...
