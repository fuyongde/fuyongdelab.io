---
title: git 恢复已删除的分支
date: 2020-03-26 17:52:01
tags:
  - git
  - 骚操作
---

在日常开发中，如果我们一不小心删除了某个分支，又想重新使用这个分支，该怎么找回该分支上的代码呢？

## 场景还原

1. 有一个 git 仓库，从 `master` 分支新建 `feature/delete_branch` 分支，并进行一次提交，如下：

```shell
# 新建一个 feature/delete_branch 分支
git branch feature/delete_branch
# 切换到 feature/delete_branch 分支
git checkout feature/delete_branch
# 新建一个文件，进行一次提交并推送至远程分支
git add delete-file.md
git commit -m ':sparkles: add a delete-file.md file'
git push origin feature/delete_branch:feature/delete_branch

# 查看当前分支图
git log --all --decorate --oneline --graph
* 88ac246 (HEAD -> feature/delete_branch, origin/feature/delete_branch) :sparkles: add a delete-file.md file
* 8693480 (origin/master, master) :sparkles: add a hello-git.md file
```

2. 切换到 `master` 分支，并删除 `feature/delete_branch` 分支

```shell
# 切换到 master 分支
git checkout master
# 删除 feature/delete_branch 分支
git branch -d feature/delete_branch
# 将删除的分支同步只至远程
git push origin --delete feature/delete_branch
# 此时无论是本地还是远程都已经删除了 feature/delete_branch
git log --all --decorate --oneline --graph
* 8693480 (HEAD -> master, origin/master) :sparkles: add a hello-git.md file
```

## 还原删除的分支

1. 通过 `git reflog` 或 `git log` 命令找到我们需要恢复的 `commit id`

```shell
# 这里演示 git reflog 命令展示出来的提交
git reflog
8693480 (HEAD -> master, origin/master) HEAD@{0}: checkout: moving from feature/delete_branch to master
88ac246 (feature/delete_branch) HEAD@{1}: commit: :sparkles: add a delete-file.md file
8693480 (HEAD -> master, origin/master) HEAD@{2}: checkout: moving from master to feature/delete_branch
8693480 (HEAD -> master, origin/master) HEAD@{3}: commit (initial): :sparkles: add a hello-git.md file

# 这里演示 git log 命令 展示出来的提交
git log -g
commit 86934803092ae7ae7266f9ebff3429437e423b80 (HEAD -> master, origin/master)
Reflog: HEAD@{0} (fuyongde <fuyongde@foxmail.com>)
Reflog message: checkout: moving from feature/delete_branch to master
Author: fuyongde <fuyongde@foxmail.com>
Date:   Thu Mar 26 18:36:35 2020 +0800

    :sparkles: add a hello-git.md file

commit 88ac246020a0cc25071fb7fd3ac88363f4bf2beb (feature/delete_branch)
Reflog: HEAD@{1} (fuyongde <fuyongde@foxmail.com>)
Reflog message: commit: :sparkles: add a delete-file.md file
Author: fuyongde <fuyongde@foxmail.com>
Date:   Thu Mar 26 18:44:01 2020 +0800

    :sparkles: add a delete-file.md file

commit 86934803092ae7ae7266f9ebff3429437e423b80 (HEAD -> master, origin/master)
Reflog: HEAD@{2} (fuyongde <fuyongde@foxmail.com>)
Reflog message: checkout: moving from master to feature/delete_branch
Author: fuyongde <fuyongde@foxmail.com>
Date:   Thu Mar 26 18:36:35 2020 +0800

    :sparkles: add a hello-git.md file

commit 86934803092ae7ae7266f9ebff3429437e423b80 (HEAD -> master, origin/master)
Reflog: HEAD@{3} (fuyongde <fuyongde@foxmail.com>)
Reflog message: commit (initial): :sparkles: add a hello-git.md file
Author: fuyongde <fuyongde@foxmail.com>
Date:   Thu Mar 26 18:36:35 2020 +0800

    :sparkles: add a hello-git.md file
```

2. 通过 `git branch [branch name] [commit id]` 命令来恢复删除的分支

```shell
# 恢复 feature/delete_branch 分支到 88ac246 版本
git branch feature/delete_branch 88ac246
# 检出 feature/delete_branch 分支
git checkout feature/delete_branch
# 此时可以看到已经删除的分支都找回来了，将分支推送的远程
git push origin feature/delete_branch:feature/delete_branch
```

## 原因

当我们删除一个分支时，git 只是删除了指向相关提交的指针，但该提交对象依然会留在版本库中，因此，如果我们知道删除分支时的 `commit id`，就可以将某个删除的分支恢复过来。
