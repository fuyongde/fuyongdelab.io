---
title: Docker 手册
date: 2019-11-14 17:31:52
categories:
  - 手册
tags:
  - Docker
---

## 镜像管理

### 查询所需的镜像

- Usage：

  `docker search [OPTIONS] TERM`

- Options：

  `-f --filter` ：根据提供的条件过滤输出

  `--format` ：使用 Go 模板进行漂亮的打印搜索

  `--limit` ：最大搜索结果数（默认为 25）

  `--no-trunc`：不要截断输出

- Example：

```shell
docker search mysql
```

### 安装镜像

- Usage：

  `docker pull [OPTIONS] NAME[:TAG|@DIGEST]`

- Options：

  `-a --all-tags`：下载存储库中的所有标记镜像

  `--disable-content-trust`：跳过图像验证（默认为 true）

- Example：

  ```shell
  # 安装MySQL的5.7.24版本
  docker pull mysql:5.7.24
  # 缺省TAG时默认安装最新版本
  docker pull mysql
  # 安装所有版本
  docker pull -a mysql
  # 安装zookeeper
  docker pull zookeeper
  ```

### 查看镜像信息

Usage：

`docker images [OPTIONS] [REPOSITORY[:TAG]]`

Options:

`-a, --all`：展示全部镜像
​ `--digests`：展示镜像的哈希值
`-f, --filter filter`：根据提供的条件过滤输出
​ `--format string`：使用 Go 模板的漂亮打印图像
​ `--no-trunc`：不要截断输出
`-q, --quiet`：仅显示数字 ID

Example:

```shell
# 查看所有的进行信息
docker images -a
```

### 删除镜像

Usage：

`docker rmi [OPTIONS] IMAGE [IMAGE...]`

Options:
`-f, --force`：强制删除某个镜像
`--no-prune` ：不删除未标记的父类

Example：

```shell
# 删除id为cd14cecfdb3a的镜像
docker rmi cd14cecfdb3a
# 删除jenkins镜像
docker rmi jenkins
```

## 容器管理

### 查看容器

```shell
# 查看所有正在运行的容器
docker ps
# 查看所有容器
docker ps -a
查看所有容器ID
docker ps -a -q
```

### 启动容器

`docker run IMG`

Example：

```shell
# 启动MySQL
docker run -p 3306:3306 --name MySQL -e MYSQL_ROOT_PASSWORD=fuyongde mysql:5.7.24
# 启动zookeeper
docker run -p 2181:2181 --name zookeeper zookeeper
```

### 停止容器

`docker stop [containerId]`：停止容器

Example:

```shell
# 删除id为8fcc3dad236a的容器
docker rm 8fcc3dad236a
```

### 重启容器

```shell
# 重启id为1e4b2a31028d的容器
docker restart 1e4b2a31028d
```

### 删除容器

`docker rm [containerId]`

```shell
# 删除id为1e4b2a31028d的容器
docker rm 1e4b2a31028d
```

### 重命名容器

```shell
# 将docker-mysql重命名为mysql
docker rename docker-mysql mysql
```

## 其他命令

### 进入到一个容器

通常我们使用的 docker 镜像一般是基于 `Linux`，则可以通过下面的方式进入到 docker 容器内:

`docker exec -it 063caee1d235 /bin/bash`

但是对于`Alpinelinux`，则需：

`docker exec -it 063caee1d235 sh`

对于部分镜像默认用户不是 `root` 的（比如 `Jenkins` 官方提供的 docker 镜像），但我们又需要使用 `root` 账户的，可以通过添加 `-u root` 参数来使用 `root` 账户登录 docker 容器：

`docker exec -it -u root a07fe9b55ade bash`

### 拷贝文件到一个容器

`docker cp C:\Users\fuyon\Documents\repo\github\goku\atomikos-demo\zhifubao-server\src\main\resources\sql\zhifubao.sql 1e4b2a31028d:/tmp`

### 从容器拷贝文件到主机

`docker cp 1e4b2a31028d:/tmp/zhifubao.sql C:\Users\fuyon\Documents`
