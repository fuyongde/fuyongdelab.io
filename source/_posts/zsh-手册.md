---
title: zsh 手册
date: 2021-08-24 22:01:14
categories:
  - 手册
tags:
  - shell
  - bash
  - zsh
---

zsh 可能是目前最好用的终端。

## 安装

### Mac OS

MacOS 自带 zsh，并且当前最新版本 MasOS 默认终端即为 zsh

### Ubuntu

使用 `sudo apt install zsh` 命令即可安装 zsh

### CentOS

使用 `sudo yum install zsh` 命令即可安装 zsh

在安装好 zsh 之后，第一次使用的时候，会有如下的提示：

```text
This is the Z Shell configuration function for new users,
zsh-newuser-install.
You are seeing this message because you have no zsh startup files
(the files .zshenv, .zprofile, .zshrc, .zlogin in the directory
~).  This function can help you with a few settings that should
make your use of the shell easier.

You can:

(q)  Quit and do nothing.  The function will be run again next time.

(0)  Exit, creating the file ~/.zshrc containing just a comment.
     That will prevent this function being run again.

(1)  Continue to the main menu.

(2)  Populate your ~/.zshrc with the configuration recommended
     by the system administrator and exit (you will need to edit
     the file by hand, if so desired).

--- Type one of the keys in parentheses ---
```

这里我推荐直接使用选项 (2)，通过 `~/.zshrc` 文件来配置我们的 zsh。

### 查看 zsh 版本

使用 `zsh --version` 命令即可查看 zsh 的版本。

### 将 zsh 设置为默认的 shell

使用 `chsh -s /bin/zsh root` 即可将 root 用户的默认 shell 设置为 zsh，如需为其他用户设置默认的 shell，将 `root` 替换为其他用户名即可。

### 查看默认的 shell

使用 `echo $SHELL` 命令即可查看默认的 shell

### 查看当前正在使用的 shell

使用 `echo $0` 命令即可查看当前正在使用的 shell

## 使用 oh-my-zsh 美化 zsh

### 安装 oh-my-zsh

打开 oh-my-zsh 官网，直接使用官网提供的命令，即可安装 oh-my-zsh，命令如下（任选其一即可）：

```shell
# 使用 curl 下载
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# 或者使用
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

对于国内的用户，上面命令下载 install.sh 可能存在连接超时的问题，我们可以替换为 gitee 的镜像来进行下载，命令如下：

```shell
wget https://gitee.com/mirrors/oh-my-zsh/raw/master/tools/install.sh
```

下载之后，使用 `sh install.sh` 来安装 oh-my-zsh，如果在 clone oh-my-zsh 过程仍然连接超时的情况下，可以通过替换 install.sh 脚本中的仓库地址来进行加速。

将脚本中的如下内容

```
REPO=${REPO:-ohmyzsh/ohmyzsh}
REMOTE=${REMOTE:-https://github.com/${REPO}.git}
```

替换为

```
REPO=${REPO:-mirrors/oh-my-zsh}
REMOTE=${REMOTE:-https://gitee.com/${REPO}.git}
```

### 配置 oh-my-zsh 主题

修改 `~/.zshrc` 文件，找到 ZSH_THEME 的选项，修改为自己喜欢的，笔者喜欢使用 ys 主题，其配置如下：

```
ZSH_THEME="ys"
```

如果你比较喜欢每次打开终端都体验不一样的主题的话，可以使用 random 主题。

### 安装常用的插件

下面列举一些非常实用的插件

- zsh-syntax-highlighting 有时候记不住一些命令，等输入完了才知道命令输错了，这个插件直接在输入过程中就会提示你，当前命令是否正确，错误红色，正确绿色

- zsh-autosuggestions 该插件会记录你之前输入过的所有命令，并且自动匹配你可能想要输入命令，然后按 → 补全

可以使用如下命令来安装插件

```
# 安装 zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# 安装 zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

```

修改 ~/.zshrc 中插件的配置

```
plugins=(git zsh-syntax-highlighting zsh-autosuggestions)
```

然后使用 `source ~/.zshrc` 来使配置生效
