---
title: android command line 安装时的坑
date: 2020-03-24 11:38:28
tags:
---

很久没有搞过 Android 开发了，Android 的命令行安装方式相比之前发生了一些变化，这里针对安装最新的 Android 命令行时遇到的一些问题，记录一下解决方法。

_问题：下载最新的 commandlinetools 在自己指定的目录安装之后，无法使用_

**原因：最新版本的 commandlinetools 对安装的目录是有要求的**

1. Download latest Command line tools from android i.e. commandlinetools-win-6200805_latest.zip
2. Unzip the downloaded file
3. Create directory for storing commandline tools somewhere on your disk, with following path included: android/cmdline-tools/latest Basically when You unzip this Cmd line tools, just rename tools directory to latest and make sure You put this latest folder in android/cmdline-tools directory somewhere on your disk
4. Create ANDROID_HOME environment variable for directory that stores the cmdline tools directory location like: `C:\YourLocationWhereYouStoreTheDirectory\android\cmdline-tools\latest`
5. Create new entry in Path environment variable as %ANDROID_HOME%\bin
