---
title: pip-手册
date: 2020-03-20 23:42:33
tags:
  - pip
  - Python
---

PyPI (Python Package Index) 是 `Python` 编程语言的软件存储库。开发者可以通过 PyPI 查找和安装由 `Python` 社区开发和共享的软件，也可以将自己开发的库上传至 PyPI 。

由于国外访问比较慢，我们可以通过配置国内的镜像来进行加速访问。

## 配置 Aliyun 的镜像

### Linux 平台

1. 找到下列文件

```shell
~/.pip/pip.conf
```

2. 在上述文件中添加或修改:

```shell
[global]
index-url = https://mirrors.aliyun.com/pypi/simple/

[install]
trusted-host=mirrors.aliyun.com
```

### Windows 平台

在 `exporler` 地址栏输入 `%AppData%`，进入文件夹后，在 `pip` 文件夹下新建 `pip.ini` 文件并写入以下配置。

```ini
[global]
index-url = http://mirrors.aliyun.com/pypi/simple
trusted-host = mirrors.aliyun.com
```
