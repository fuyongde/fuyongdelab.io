---
title: javaagent 使用指南
date: 2022-01-08 21:03:22
tags:
  - Java
---

最近在做基于 JaCoCo 的代码覆盖率工具，了解到了一下 javaagent 相关的一些知识点。

## javaagent 是什么？

javaagent 是 java 命令的一个参数选项，可以用于加载 Java 语言的代理。

那么这个参数加载的 Java 语言的代理需要满足什么样的规范呢？

1. 代理 jar 包中的 MANIFEST.MF 文件需指定 Premain-Class；
2. Premain-Class 指定的类中必须实现 premain 方法。
