---
title: Linux 手册
date: 2019-09-24 10:40:15
categories:
  - 手册
tags:
  - Linux
  - 未完待续
---

记录一些常用的 Linux 操作，持续更新 ...

### 查看系统版本

常用的查看系统版本有以下命令

- `cat /etc/os-release`
- `uname -a`
- `cat /proc/version`
- `cat /etc/issue`

下面演示一下 `cat /etc/os-release`，其他不详细说明。

```shell
cat /etc/os-release
```

输出如下

```text
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

### 替换源

由于网络的原因，国内部分 Linux 在安装软件时非常缓慢，将源替换为国内的镜像可以方便很多。

国内常见的镜像如清华大学、阿里云、163 等等，大家可自行选择。由于大部分网络上的博客，只写了具体源的地址，对很多没有基础的同学来说，由于系统版本的差异，反而会适得其反。

替换源一定要遵循下面的步骤：

1. 确认操作系统版本。如何查看可参考上文。
2. 备份默认的源。不同的操作系统版本，源的文件位置各不一样，直接采用备份文件的方式备份即可。
3. 选择对应版本的源并替换。

下面针对常见的 `Debian`、`Ubuntu`、`CentOS`，这里以说明一下如何正确的查找并替换源。

#### Debian

`Debian` 系统的源文件目录位于 `/etc/apt/sources.list`，这里我们以清华大学的镜像为例。清华大学开源镜像站 [Debian 系统的帮助页面](https://mirror.tuna.tsinghua.edu.cn/help/debian/) 详细列举了常用版本的源的信息。

例如，我们系统的版本是 `9 (stretch)`，则选择 `stretch`，替换掉对应的源即可。

stretch 版本的源信息如下：

```text
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch-updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch-updates main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch-backports main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ stretch-backports main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian-security stretch/updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian-security stretch/updates main contrib non-free
```

buster 版本的源信息如下

```text
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-updates main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian-security buster/updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian-security buster/updates main contrib non-free
```

其他版本不一一列举。

#### Ubuntu

`Ubuntu` 也是基于 `Debian` 的，故源的目录位置也是 `/etc/apt/sources.list`。清华大学开源镜像站 [Ubuntu 系统的帮助页面](https://mirror.tuna.tsinghua.edu.cn/help/ubuntu/) 详细列举了常用版本的源的信息。

例如，我们系统的版本是 `18.04 LTS`，则选择 `18.04 LTS`，替换掉对应的源即可。

18.04 LTS 版本的源信息如下：

```text
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-backports main restricted universe multiverse
deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-security main restricted universe multiverse

# 预发布软件源，不建议启用
# deb https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
# deb-src https://mirrors.tuna.tsinghua.edu.cn/ubuntu/ bionic-proposed main restricted universe multiverse
```

`Ubuntu` 若安装了图形界面，也可以采用图形界面的方式。

##### 图形界面配置

新手推荐使用图形界面配置： 系统设置 -> 软件和更新 选择下载服务器 -> "mirrors.aliyun.com"

#### CentOS

`CentOS` 的源文件位于 `/etc/yum.repos.d/CentOS-Base.repo`，清华大学开源镜像站 [CentOS 系统的帮助页面](https://mirror.tuna.tsinghua.edu.cn/help/centos/) 详细列举了常用版本的源的信息。

与 Debian 的不同，CentOS 替换源之后，需要运行 `yum makecache` 生成缓存。

此外阿里云提供了更为简单的方式，直接下载阿里云的 `CentOS-Base.repo` 文件到对应目录即可，命令如下：

```shell
# CentOS 6
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo
# 或者
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-6.repo

# CentOS 7
wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
# 或者
curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo

# 由于非阿里云 ECS 用户会出现 Couldn't resolve host 'mirrors.cloud.aliyuncs.com' 信息，不影响使用。用户也可自行修改相关配置: eg:
sed -i -e '/mirrors.cloud.aliyuncs.com/d' -e '/mirrors.aliyuncs.com/d' /etc/yum.repos.d/CentOS-Base.repo
```

CentOS 7 版本的清华大学源信息如下：

```text
# CentOS-Base.repo
#
# The mirror system uses the connecting IP address of the client and the
# update status of each mirror to pick mirrors that are updated to and
# geographically close to the client.  You should use this for CentOS updates
# unless you are manually picking other mirrors.
#
# If the mirrorlist= does not work for you, as a fall back you can try the
# remarked out baseurl= line instead.
#
#

[base]
name=CentOS-$releasever - Base
baseurl=https://mirrors.tuna.tsinghua.edu.cn/centos/$releasever/os/$basearch/
#mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=os
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#released updates
[updates]
name=CentOS-$releasever - Updates
baseurl=https://mirrors.tuna.tsinghua.edu.cn/centos/$releasever/updates/$basearch/
#mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=updates
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras
baseurl=https://mirrors.tuna.tsinghua.edu.cn/centos/$releasever/extras/$basearch/
#mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=extras
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7

#additional packages that extend functionality of existing packages
[centosplus]
name=CentOS-$releasever - Plus
baseurl=https://mirrors.tuna.tsinghua.edu.cn/centos/$releasever/centosplus/$basearch/
#mirrorlist=http://mirrorlist.centos.org/?release=$releasever&arch=$basearch&repo=centosplus
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```

### 配置免密登录

场景：A 主机实现免密登录 B 主机

1. A 主机生成公私钥对 `ssh-keygen`；
2. 将生成的公钥传输给 B 机器 `scp /root/.ssh/id_rsp.pub remoteUser@remoteIp:remoteFolder`；
3. 登录 B 机器，将 A 机器的公钥追加到 B 主机用户目录下的 `.ssh/authorized_keys` 文件，执行 `cat id_rsa.pub >> .ssh/.ssh/authorized_keys` 命令即可。若没有`authorized_keys`文件，则需要创建，且其权限要为 `600`。

### 查看系统负载

`top`：重点关注 CPU、内存的使用率，以及 average（平均负载，三个值分别是 CPU 最近 1 分钟、5 分钟、15 分钟的平均负载）

`vmstat`：

### Linux 子系统

在 `Windows 10` 中使用 Linux 子系统时，经常会需要用到 `root` 账号，初始情况下，该账号时没有设定密码的，需要设定之后才能正常使用。

```shell
# 设置 root 账户的密码
sudo passwd
# 切换 root 账户
su
```

### 软件管理

#### Ubuntu 软件管理

##### 查看安装了那些软件

```shell
# 查看安装了哪些软件包
apt list --installed
# 也可以使用 dpkg 命令来查看安装了哪些软件包
dpkg --list
```

##### 卸载已经安装的软件

```shell
# 卸载某个软件包
apt remove packagename
# 也可以使用自动卸载
apt autoremove packagename
# 使用 dpkg 命令卸载
dpkg --remove packagename
```
