---
title: Gradle 手册
date: 2020-03-08 19:46:14
categories:
  - 手册
tags:
  - Gradle
  - 构建工具
  - 未完待续
---

## Gradle 是什么

> Gradle is an open-source build automation tool focused on flexibility and performance. Gradle build scripts are written using a Groovy or Kotlin DSL. Read about Gradle features to learn what is possible with Gradle.

Gradle 是一个专注于灵活性和性能的开源自动化构建工具，其脚本由 `Groovy` 或 `Kotlin DSL` 编写。Gradle 有以下特性：

- 高度可定制：Gradle 采用可以高度自定义的方式进行建模。
- 快速：Gradle 通过重用旧的输出、仅变更的输入以及并行的方式来快速完成任务。
- 功能强大：Gradle 是 `Android` 的官方构建工具，并附带对许多流行语言和技术的支持。

## 安装

安装之前，请确保已经正确安装 `JDK8` 或更高版本

### 使用包管理器安装

针对类 `Unix` 系统（`macOS`、`Linux`、`Cygwin`、`Solaris` 和 `FreeBSD`）可以采用以下两种包管理的方式安装。

- SDKMAN 包管理器

```shell
sdk install gradle
```

- Homebrew 包管理器

```shell
brew install gradle
```

### 手动安装

1. 下载指定的 Gradle 发行版，下载地址可参考[发布页](https://gradle.org/releases/)
2. 解压缩到指定的目录，如 Windows 系统中的 `C:\gradle` 或 Linux 系统中的 `/opt/gradle` 等
3. 配置环境变量

- 针对 `Windows` 系统，打开 `控制面板` > `系统和安全` > `系统` > `高级系统设置` > `环境变量` > `系统变量`，新增 `GRADLE_HOME` 变量，该变量指向 Gradle 的安装路径，然后选中`系统变量`中的 `Path`，并新增 `%GRADLE_HOME%\bin\`
- 针对 `Linux` 系统，略

除了配置环境变量之外，还可以配置 `GRADLE_USER_HOME` 变量来指定 Gradle 的库文件的目录，该变量缺省值为当前系统用户目录下的 `.gradle` 目录。

### 验证

在命令行中执行 `gradle -v` 命令，若输出为你安装的版本信息，则说明安装成功了。

## 使用 Gradle 构建一个项目

1. 新建一个目录 `mkdir hello-world`
2. 进入该目录 `cd hello-world`
3. 使用 Gradle 初始化该目录 `gradle init`。执行完该命令之后按照指引选择要初始化工程的类型等。之后可以得到一个至少包含以下路径的目录结构。

```
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
└── settings.gradle
```

看到 `gradlew`、`gradlew.bat` 以及 `wapper` 目录是不是有点熟悉？没错，这个和 Maven Wapper 的思想是一样的，可参考 [Maven Wapper 手册](https://fuyongde.github.io/2019/06/23/maven-wapper-手册/)这篇文章。

未完待续...
