---
title: PowerShell 手册
date: 2019-09-27 11:24:44
categories:
  - 手册
tags:
  - PowerShell
  - 未完待续
---

记录一些常见的 `Powershell` 的使用问题

## 别名命令

在使用 Linux 系统时，我喜欢使用 `ll` 命令来查看当前目录的文件，但是 Windows 系统的 Powershell 没有这个命令，所以每次用的时候就很不习惯。我们可以通过对命令设置别名来让 Powershell 支持我们的使用习惯。

### 查看别名

`Get-Alias` 或者 `gal` 命令来查看我们系统命令的别名，如下

```powershell
PS C:\Users\fuyongde> get-alias

CommandType     Name                         Version    Source
-----------     ----                         -------    ------
Alias           % -> ForEach-Object
Alias           ? -> Where-Object
Alias           ac -> Add-Content
Alias           cat -> Get-Content
```

- 查看某别名的原命令，如 `ls` 的原命令：`Get-Alias ls`
- 查看某原命令的别名，如 `Get-ChildItem` 的别名：`Get-Alias -Definition Get-ChildItem`

### 临时创建或更改别名

使用 `Set-Alias` 命令来创建或更改别名，但是改命令只在当前的的 PowerShell Session 中生效。另外还有一个 `New-Alias` 命令用法类似。

#### 创建不带参数的别名

```powershell
PS C:\Users\fuyongde> Set-Alias -Name ll -Value get-childitem
# 或者简单一些
PS C:\Users\fuyongde> Set-Alias ll get-childitem
```

#### 创建带参数的别名

如果命令带参数如想为 `Get-ChildItem -Name` 设定别名为 `ll` 则我们需要两步，第一步为这个带参数原命令设定一个中间 `function`，第二步为这个 `function` 指定别名

```powershell
PS C:\Users\fuyongde> function getlist {Get-ChildItem -Name}
PS C:\Users\fuyongde> Set-Alias ll getlist
```

#### 创建永久的别名

在 PowerShell 中直接使用 `Set-Alias` 或 `New-Alias` 命令创建的别名在关闭此 Session 后即会失效，防止此现象的方法是将此命令写入 Windows PowerShell profile 文件。

查看此文件在计算机中的位置：

```powershell
PS C:\Users\fuyongde> $profile
```

结果如下：

`C:\Users\fuyongde\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`

但如果安装了 `PowerShell6` 的话，则结果是：

`C:\Users\fuyongde\Documents\PowerShell\Microsoft.PowerShell_profile.ps1`

打开对应文件，并写入要预设的别名

```powershell
Set-Alias ll get-childitem
```

注意：该种方式有可能会遇到系统上禁止运行脚本的提示，这是因为 PowerShell 对安全做过充分考量，把脚本的执行分成了几个策略。以下是常用的执行策略：

- `Restricted`：禁止运行任何脚本和配置文件，默认情况下就是该策略。
- `AllSigned`：可以运行脚本，但要求所有脚本和配置文件由可信任的发布者签名，包括在本地计算机编写的脚本。
- `RemoteSigned`：可以运行脚本，但要求从网络上下载的脚本和配置由可信任的发布者签名，不要求对已经运行和在本地计算机编写的脚本进行签名。
- `Unrestricted`：可以运行未签名的脚本，这种策略也是最危险的。

通过 `Get-ExecutionPolicy` 命令可查看当前配置的执行策略，通过管理员运行 PowerShell 并执行 `Set-ExecutionPolicy RemoteSigned` 可更改执行策略。

### 删除别名

使用 `Remove-Item alias` 命令删除已设定的别名。

如删除别名 ll ：

```powershell
PS C:\Users\fuyongde> Remove-Item alias:\ll
```

## 环境变量命令

### 切换 PowerShell 的 page 为 UTF-8 代码页

在最新版本的 windows 操作系统中，powershell 默认的代码页已经是 UTF-8 字符集的代码页了。常见的代码页如下：

- `65001`：UTF-8 代码页

- `950`：繁体中文

- `936`：简体中文默认的 GBK

- `437`：MS-DOS 美国英语

命令如下：

```powershell
chcp 65001
```

## Modules 管理

### 安装 Module

使用 `Install-Module` 命令来安装 Module，其支持的参数有很多，具体可以查看[官方文档](https://docs.microsoft.com/zh-cn/powershell/module/powershellget/install-module)，这里我们只列举最常用的参数的使用方法：

```powershell
# 为当前用户安装 posh-git 模块
Install-Module -Name posh-git -Scope CurrentUser
```

### 查找 Module

使用 `Find-Module` 命令可以来查找 Module，其支持的参数有很多，具体可以查看[官方文档](https://docs.microsoft.com/zh-cn/powershell/module/powershellget/find-module)，这里我们只列举最常用的参数的使用方法：

```powershell
# 查找名称为 PowerShellGet 的 Module
Find-Module -Name PowerShellGet
```

### 查看已经安装的 Modules

```powershell
# 查看已经安装的所有的 Modules
Get-InstalledModule
# 查看已经安装的指定名称、版本的 Modules
Get-InstalledModule -Name "AzureRM.Automation" -MinimumVersion 1.0 -MaximumVersion 2.0
```

### 更新已经安装的 Modules

使用 `Install-Module` 命令来安装 Module，其支持的参数有很多，具体可以查看[官方文档](https://learn.microsoft.com/zh-cn/powershell/module/powershellget/update-module)，这里我们只列举最常用的参数的使用方法：

```powershell
# 以 post-git 为例，使用 Update-Module 来更新安装的 posh-git 版本
Update-Module -Name posh-git
# 将模块更新到指定的版本
Update-Module -Name SpeculationControl -RequiredVersion 1.0.14
```

### 卸载 Module

使用 `Uninstall-Module` 命令可以来卸载 Module，其支持的参数有很多，具体可以查看[官方文档](https://docs.microsoft.com/zh-cn/powershell/module/powershellget/uninstall-module?view=powershell-7.2)，这里我们只列举最常用的参数的使用方法：

```powershell
# 卸载指定名称的模块
Uninstall-Module -Name SpeculationControl
# 强制卸载指定名称的模块
Uninstall-Module -Name SpeculationControl -Force
# 使用流水线的方式卸载，即先查找到某个模块并卸载
Get-InstalledModule -Name SpeculationControl | Uninstall-Module
# 卸载指定版本的模块
Uninstall-Module -Name 'oh-my-posh' -RequiredVersion '7.31.1'
```

## 美化

PowerShell 原始的界面是非常丑的，我们可以通过安装一些 modules 来进行美化，网上有很多博客介绍如何美化 PowerShell，但是大部分都没有介绍每个 module 的作用。

### oh-my-posh

> A prompt theme engine for any shell.

从介绍中可以看出 oh-my-posh 是为任何 shell 提供的一套主题引擎，这个是美化 PowerShell 的关键，具体更多的文档可以查阅[官网](https://ohmyposh.dev/)。

#### 安装 oh-my-posh

最新版本的 oh-my-posh 已经作为一个独立的应用来安装了（旧版本是作为 PowerShell 的一个 Module 来进行安装的），这里我们使用 winget 命令来安装最新版本的 oh-my-posh ，安装命令如下：

```powershell
winget install JanDeDobbeleer.OhMyPosh -s winget
```

安装完成之后，需要将 oh-my-posh 添加到环境变量的 path 里面

````powershell
$env:Path += ";C:\Users\user\AppData\Local\Programs\oh-my-posh\bin"
```$

#### 更新 oh-my-posh

如果之前安装过 oh-my-posh 但是想要更新版本，其更新命令如下：

```powershell
winget upgrade JanDeDobbeleer.OhMyPosh -s winget
````

#### 启用 oh-my-posh

安装 oh-my-posh 之后，并不会直接启用，跟上文创建别名的方法一样，在 Windows PowerShell profile 文件中启用即可。通过 `notepad $PROFILE` 命令，打开 Windows PowerShell profile 文件，在文件中添加如下内容：

```powershell
# 使用 oh-my-posh 来初始化 powershell
oh-my-posh init pwsh | Invoke-Expression
# 指定使用的主题
oh-my-posh init pwsh --config "$env:POSH_THEMES_PATH\yyds.omp.json" | Invoke-Expression
```

#### 部分主题乱码问题

由于一些 oh-my-posh 中用到了一些特殊的字符，需要对应的终端更换为支持这种特殊字符的字体，可以在[nerdfonts 网站](https://www.nerdfonts.com/)下载自己喜欢的字体，个人推荐 `CaskaydiaMono Nerd Font Mono` 这个字体。

### posh-git

posh-git 用于在 shell 上提示 git 的一些信息，其安装、更新、卸载过程跟其他的 PowerShell modules 一致。这里只列举安装命令：

```powershell
Install-Module posh-git -Scope CurrentUser
```

#### 启用 posh-git

安装 posh-git 之后，并不会直接启用，跟上文创建别名的方法一样，在 Windows PowerShell profile 文件中启用即可。通过 `notepad $PROFILE` 命令，打开 Windows PowerShell profile 文件，在文件中添加如下内容：

```powershell
# 引入 oh-my-posh 模块
Import-Module posh-git
```

### PSReadLine

PSReadLine 在 PowerShell 控制台中提供改进的命令行编辑体验。具体说明可参考其[官方文档](https://docs.microsoft.com/zh-cn/powershell/module/psreadline/about/about_psreadline)。

#### 安装 PSReadLine

```powershell
# 需要注意的是：由于最新的 windows 版本自带了 PSReadLine，但是其版本较低，无法启用跟 oh-my-posh 配合的功能，故我们采用为当前用户强制安装新版本的方式
Install-Module -Name PSReadLine -Scope CurrentUser -Force
```

#### 启用 PSReadLine

安装 PSReadLine 之后，并不会直接启用，跟上文创建别名的方法一样，在 Windows PowerShell profile 文件中启用即可。通过 `notepad $PROFILE` 命令，打开 Windows PowerShell profile 文件，在文件中添加如下内容：

```powershell
# 引入 PSReadLine 模块
Import-Module PSReadLine
# 设置编辑模式为 Emacs
Set-PSReadlineOption -EditMode Emacs
# 开启预测 IntelliSense，其模式为历史命令，即历史敲过的命令都会进行提示
Set-PSReadLineOption -PredictionSource History
# 开启预测 IntelliSense 之后，使用快捷键快速选中命令
Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward
```
