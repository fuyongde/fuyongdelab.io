---
title: Jib - Make docker simple again
date: 2019-06-26 17:03:45
tags:
  - Docker
  - Jib
  - Spring Boot
---

在 [使用 Docker 构建 Spring Boot Application](https://fuyongde.github.io/2019/06/25/Dockerizing-Spring-Boot-Application/) 中简单介绍了如何使用 `Docker` 构建 `Spring Boot Application`。本文探讨如何更简单的构建 `Docker` 镜像。

## jib 简介

> Make America Great Again. -- Donald Trump

大概是受 `Donald Trump` 的启发 🙄，Google 发布的 `jib` 让原本已经很简单的 `Docker` 镜像构建过程变得更加简单。

一图顶千言，先看 `Docker` 构建镜像流程

{% asset_img docker_process.jpg Docker 构建容器镜像的流程 %}

再看 `Jib` 构建容器镜像流程

{% asset_img jib_process.jpg Jib 构建容器镜像的流程 %}

## 使用

下面来介绍以下如何使用 `Jib`

### 编写 Spring Boot Application

略

### 配置 Jib 插件

在项目的 `pom.xml` 文件中，新增如下配置

```xml
<plugin>
    <groupId>com.google.cloud.tools</groupId>
    <artifactId>jib-maven-plugin</artifactId>
    <version>1.3.0</version>
    <configuration>
        <from>
            <image>openjdk:8u212-jdk-alpine</image>
        </from>
        <to>
            <image>registry.cn-hangzhou.aliyuncs.com/fuyongde/ebony-maw:${project.version}</image>
        </to>
        <container>
            <jvmFlags>
                <jvmFlag>-Xms128m</jvmFlag>
                <jvmFlag>-Xmx128m</jvmFlag>
            </jvmFlags>
        </container>
    </configuration>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>build</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

其中

- from：等同于 `Dockerfile` 中的 `FROM`
- to：镜像容器服务的地址以及对应的 `tag`，这里我采用的是阿里云的地址
- jvmFlag：容器启动时的 `JVM` 参数

### 构建

在终端执行 `maven package` 命令，`Docker` 镜像就打包并上传好了 ✌。

至此，整个构建过程已经完成，我们甚至连 `Dockerfile` 都不用写，是不是超级简单 🤪。

如有需要，可参考本文中的 [Demo](https://github.com/fuyongde/marvel/tree/master/ebony-maw) 。
