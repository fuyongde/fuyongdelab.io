---
title: curl-command 手册
date: 2019-10-24 10:40:15
categories:
  - 手册
tags:
  - curl
  - command
  - 未完待续
---

curl 是十分常用的命令，通常我们用这个命令来请求 Web 服务器。一些常见的构造 HTTP 请求的客户端（如 Postman）关于项目协作的功能都是商业化版本才有的，团队开发过程中通过维护接口的 curl 命令脚本可以低成本的代替一些商业化软件的功能。

下面介绍一下 curl 命令的常见用法。

## 基础用法

### 请求一个 URL

```shell
curl http://www.baidu.com/
```

### -X

使用 -X 参数可以指定 HTTP Method

```shell
curl -X POST http://www.baidu.com/
```

### -H, --header

使用 -H 或者 --header 参数可以添加 HTTP 的请求头

```shell
curl -H "Content-Type:application/json" http://www.baidu.com/
```

### -d, --data

使用 -d 或 --data 可以发送 HTTP 的请求参数，可以是 form 表单，也可以是二进制的 request body

```shell
# 发送 form 表单
curl -X POST -d "name=jasonfu&age=20&birthday=1990/06/01" "http://localhost:8080/http/post?type=form"

# 也可以使用多个 -d 参数
curl -X POST -d "name=jasonfu" -d "age=20" -d "birthday=1990/06/01" "http://localhost:8080/http/post?type=form"

### 发送 request body
curl -X POST -H "Content-Type:application/json" -d "{\"name\":\"jasonfu\",\"age\":19,\"birthday\":12345678911}" "http://localhost:8080/http/post?type=body"
```

### -F, --form

使用 -F 或 --form 参数可以上传文件

```shell
### 上传文件
curl -F "file=@alias.bat" -X POST "http://localhost:8080/http/post?type=upload"
### 上传文件 带参数
curl --form "file=@alias.bat" --form "id=1" -X POST "http://localhost:8080/http/post?type=upload"
```

## 进阶用法

### -i, -I

使用 -i 参数可以输出 HTTP 的相应头

```shell
curl -i http://www.baidu.com/
```

使用 -I 参数只输出 HTTP 的相应头

```shell
curl -I http://www.baidu.com/
```

### -e, --referer

使用 -e 或者 --referer 参数可以在 HTTP 请求头中添加 referer 字段，标识从哪里跳转过来的

```shell
curl --referer http://www.baidu.com/ http://www.qq.com/
```

### -A, --user-agent

使用 -A 或者 --user-agent 参数可以指定 HTTP 请求头中的 User-Agent 字段

```shell
curl --user-agent curl-client http://www.baidu.com/
```

### cookie

使用 -b 或者 --cookie 可以发送 cookie 给 Web 服务器。

```shell
curl --cookie "user=fuyongde" http://www.baidu.com/
```

使用 -c 参数可以保存服务端返回的 cookie 到指定的文件

```shell
curl -c baidu-cookie.txt http://www.baidu.com/
```

### 显示通信过程

使用 -v 或者 --verbose 参数可以展示完整的 HTTP 通信过程

```shell
curl -v http://www.baidu.com/
```

使用 --trace 或者 --trace-ascii 参数可以展示更详细的通信过程

```shell
curl --trace tract.txt http://www.baidu.com/
```

### 保存响应

使用 -o 或者 -O 参数可以将 http 相应保存为文件，区别在于 -o 可以指定文件名，-O 则是使用 URL 中最后一段 path 作为文件名来进行保存，其下载文件的效果等同于 `wget` 命令。

```
# 将 http://www.baidu.com/ 响应保存至文件 baidu.html 中
curl -o baidu.html http://www.baidu.com/
# 将 http://localhost:8080/ping 响应保存至文件 ping 中
curl -O http://localhost:8080/ping
```

### 使用代理

一些 Web 服务器是仅支持某些特定的 ip 来访问的，这种情况下，我们就需要使用代理来发送我们的 HTTP 请求，使用 -x 参数可以指定代理服务器来发送我们的请求。

```shell
# 使用 socks5 协议的代理服务器来发送请求
curl -x socks5://username:password@proxyhost:8080 http://www.baidu.com/
# 默认情况下使用 http 协议的代理服务器来发送请求
curl -x username:password@proxyhost:8080 http://www.baidu.com/
```

### -L, --location

对于一些 HTTP 响应码为 302 的情况，可以使用 -L 或者 --location 参数来实现自动跳转的效果

```shell
curl -L -i http://localhost:8080/http/redirect
```

其输出如下：

```
HTTP/1.1 302
Location: http://localhost:8080/http/get/jasonfu
Content-Length: 0
Date: Tue, 24 Aug 2021 10:22:10 GMT

HTTP/1.1 200
Content-Type: application/json
Transfer-Encoding: chunked
Date: Tue, 24 Aug 2021 10:22:10 GMT

{"name":"jasonfu","age":18,"birthday":"2021-08-24T10:22:10.713+00:00"}
```

可以看到已经实现了自动跳转的效果

### --limit-rate

使用 --limit-rate 参数可以限制访问 HTTP 请求和响应的带宽。

```shell
# 将带宽限制为每秒 1k 字节
curl --limit-rate 1k http://www.baidu.com/
```
